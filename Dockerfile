FROM node:alpine

RUN mkdir /src
WORKDIR /src
VOLUME ["/src"]

ENTRYPOINT ["npm"]

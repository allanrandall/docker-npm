# npm

This repo is to build a docker container to expose npm without having to install node
on my local system.

Usage:
```
docker run -v ${PWD}:/src npm run build
```
